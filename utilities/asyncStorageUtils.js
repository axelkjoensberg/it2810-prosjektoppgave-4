import { AsyncStorage } from "react-native";

export async function storeData(key, value) {
	try {
		await AsyncStorage.setItem(key, value);
	} catch (error) {
		throw error;
	}
}

export async function getData(key) {
	try {
		const value = await AsyncStorage.getItem(key);
		return value;
	} catch (error) {
		throw error;
	}
}

export async function storeHistoryEntry(searchEntry) {
	const key = "searchHistory";
	try {
		const existingHistory = await getData(key);
		let parsedExHistory;
		if (existingHistory) {
			parsedExHistory = JSON.parse(existingHistory);
		}
		if (
			parsedExHistory &&
			parsedExHistory.length &&
			parsedExHistory[0].searchValue &&
			parsedExHistory[0].timestamp
		) {
			const newEntry = [
				{
					searchValue: searchEntry.searchValue,
					timestamp: new Date(),
				},
			];
			if (parsedExHistory[0].searchValue !== newEntry[0].searchValue) {
				const extendedHistory = parsedExHistory.concat(newEntry);
				await storeData(
					key,
					JSON.stringify(
						extendedHistory.sort((a, b) => (a.timestamp < b.timestamp ? 1 : -1))
					)
				);
			}

			const newHistory = await getData(key);
			return JSON.parse(newHistory);
		} else {
			await storeData(
				key,
				JSON.stringify([
					{
						searchValue: searchEntry.searchValue,
						timestamp: new Date(),
					},
				])
			);
			const newHistory = await getData(key);
			return JSON.parse(newHistory);
		}
	} catch (error) {
		throw error;
	}
}

export async function getHistory() {
	try {
		const history = await getData("searchHistory");
		return JSON.parse(history);
	} catch (error) {
		throw error;
	}
}

export async function storeFavorite(favorite) {
	const key = "favorites";
	try {
		const existingFavorites = await getData(key);
		let parsedExistingFavorites;
		if (existingFavorites) {
			parsedExistingFavorites = JSON.parse(existingFavorites);
		}
		if (
			parsedExistingFavorites &&
			parsedExistingFavorites.length &&
			parsedExistingFavorites[0].name &&
			favorite
		) {
			const newFavorite = [
				{
					name: favorite.name,
					_id: favorite._id,
					minutes: favorite.minutes,
				},
			];
			const found = parsedExistingFavorites.find(
				favorite => favorite._id === newFavorite[0]._id
			);
			if (!found) {
				const extendedFavorites = parsedExistingFavorites.concat(newFavorite);
				await storeData(key, JSON.stringify(extendedFavorites));
			}

			const newFavorites = await getData(key);
			return JSON.parse(newFavorites);
		} else {
			await storeData(
				key,
				JSON.stringify([
					{
						name: favorite.name,
						_id: favorite._id,
						minutes: favorite.minutes,
					},
				])
			);
			const newFavorites = await getData(key);
			return JSON.parse(newFavorites);
		}
	} catch (error) {
		throw error;
	}
}

export async function removeFavorite(favoriteToRemove) {
	const key = "favorites";
	try {
		const existingFavorites = await getData(key);
		let parsedExistingFavorites;
		if (existingFavorites) {
			parsedExistingFavorites = JSON.parse(existingFavorites);
		}
		if (
			parsedExistingFavorites &&
			parsedExistingFavorites.length &&
			parsedExistingFavorites[0].name &&
			favoriteToRemove
		) {
			const index = parsedExistingFavorites.findIndex(
				favorite => favorite._id === favoriteToRemove._id
			);
			if (index >= 0 && index < parsedExistingFavorites.length) {
				parsedExistingFavorites.splice(index, 1);
				await storeData(key, JSON.stringify(parsedExistingFavorites));
			}
			const newFavorites = await getData(key);
			return JSON.parse(newFavorites);
		}
	} catch (error) {
		throw error;
	}
}

export async function getFavorites() {
	try {
		const favorites = await getData("favorites");
		return JSON.parse(favorites);
	} catch (error) {
		throw error;
	}
}
