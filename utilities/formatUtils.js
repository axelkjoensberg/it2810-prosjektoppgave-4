/**
 * Capitalizes the first letter of a string
 * @param {String} string
 * @returns {String}
 */
export function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
/**
 * Capitalizes the first letter of every sentence in a string
 * @param {String} string
 * @returns {String}
 */
export function capitalizeSentences(string) {
	const capFirst = capitalizeFirstLetter(string);
	const splitString = capFirst.split(/[.!?;]/g);
	const capitalizedSentences = splitString.reduce((prevString, currString) => {
		return prevString.trim() + ". " + capitalizeFirstLetter(currString.trim());
	});
	return capitalizedSentences;
}

/**
 * Formats a number of minutes into a readable string
 * @param {Number} minutes
 * @returns {String}
 */
export function formatMinutes(minutes) {
	const hours = Math.floor(minutes / 60);

	if (minutes >= 60 * 24) {
		const days = Math.floor(minutes / 60 / 24);
		const leftoverHours = hours % 24;
		return `${days} ${days > 1 ? "days" : "day"}${
			leftoverHours
				? `, ${leftoverHours} ${leftoverHours > 1 ? "hours" : "hour"}`
				: ""
		}`;
	} else if (minutes >= 60) {
		const leftoverMins = minutes % 60;
		return `${hours} ${hours === 1 ? "hour" : "hours"}${
			leftoverMins
				? `, ${leftoverMins} ${leftoverMins === 1 ? "minute" : "minutes"}`
				: ""
		}`;
	} else {
		return `${minutes} ${minutes === 1 ? "minute" : "minutes"}`;
	}
}

/**
 * Capitalizes sentences and fixes various errors in a text
 * @param {String} string
 * @returns {String}
 */
export function formatText(string) {
	const cappedSentences = capitalizeSentences(string);
	const fixedCommas = cappedSentences.replace(/ , /g, ", ");
	const fixedExclamations = fixedCommas.replace(/ !/g, "!");
	const removedReduntantPeriods = fixedExclamations.replace("..", ".");
	const capitalizedIs = removedReduntantPeriods.replace(/ i /g, " I ");
	const removedRedundantSpaces = capitalizedIs.replace(/[ ]{2,}/g, " ");
	const removedExcessivePeriods = removedRedundantSpaces.replace(
		/[.]{4,}/g,
		"..."
	);
	return removedExcessivePeriods;
}

/**
 * Formats a given past date into proper language
 * @param {Date} timestampDate
 * @returns {String} formatted timestamp
 */
export function formatTimestamp(timestampDate) {
	const localDate = new Date(timestampDate);
	const now = new Date();

	const secondsAgo = Math.round((now - localDate) / 1000);
	const minutesAgo = Math.round(secondsAgo / 60);

	let timestamp = formatMinutes(minutesAgo);

	if (timestamp === "1 day") {
		timestamp = "yesterday";
	} else if (secondsAgo < 60) {
		timestamp = "just now";
	} else {
		timestamp += " ago";
	}
	return timestamp;
}

export function formatSearchValue(searchValue) {
	const formattedSearchValue = searchValue.replace(/\\/g, "");
	return formattedSearchValue;
}
