import { SET_FAVORITES } from "../actionTypes";

const initialState = {
	favorites: false,
};

const favorites = (state = initialState, action) => {
	switch (action.type) {
		case SET_FAVORITES: {
			return action.payload;
		}
		default: {
			return state;
		}
	}
};

export default favorites;