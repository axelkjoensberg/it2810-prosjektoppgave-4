import { TOGGLE_SORT_DESC, SET_SORT_FIELD } from "../actionTypes";

const initialState = {
	sortField: "name",
	sortDESC: false,
};

const recipeSort = (state = initialState, action) => {
	switch (action.type) {
		case SET_SORT_FIELD: {
			return { ...state, sortField: action.payload.sortField };
		}
		case TOGGLE_SORT_DESC: {
			return { ...state, sortDESC: !state.sortDESC };
		}
		default: {
			return state;
		}
	}
};
export default recipeSort;
