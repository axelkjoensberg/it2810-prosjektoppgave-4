import {
	SET_MAX_INGREDIENTS,
	SET_MAX_MINUTES,
	SET_MAX_STEPS
} from "../actionTypes";

const initialState = {
	minIngredients: false,
	maxIngredients: 0,
	minSteps: false,
	maxSteps: 0,
	minMinutes: false,
	maxMinutes: 0
};

const recipeFilters = (state = initialState, action) => {
	switch (action.type) {
		case SET_MAX_INGREDIENTS: {
			return {
				...state,
				maxIngredients: action.payload.maxIngredients
			};
		}
		case SET_MAX_MINUTES: {
			return {
				...state,
				maxMinutes: action.payload.maxMinutes
			};
		}
		case SET_MAX_STEPS: {
			return {
				...state,
				maxSteps: action.payload.maxSteps
			};
		}
		default: {
			return state;
		}
	}
};
export default recipeFilters;
