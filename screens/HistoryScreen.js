import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import {
	Container,
	Content,
	Text,
	List,
	ListItem,
	Body,
	Right,
	H1,
} from "native-base";
import { connect } from "react-redux";

import { getHistory } from "../utilities/asyncStorageUtils";
import {
	setHistory,
	setSearchValue,
	setLoadModeLoad,
	resetPage,
} from "./../redux/actions";
import { formatTimestamp, formatSearchValue } from "./../utilities/formatUtils";

const HistoryScreen = props => {
	useEffect(() => {
		getHistory()
			.then(history => props.dispatch(setHistory(history)))
			.catch(error => console.error(error));
	}, []);

	function handleEntryTap(searchValue) {
		const stringWithoutBackslash = searchValue.replace(/\\/g, "");
		const stringWithEscapedQuotes = stringWithoutBackslash.replace(/"/g, '\\"');
		props.dispatch(setSearchValue(stringWithEscapedQuotes));
		props.dispatch(setLoadModeLoad());
		props.dispatch(resetPage());
		props.navigation.navigate("Search");
	}

	return (
		<Container>
			<Content>
				<H1 style={styles.title}>Search history</H1>
				{props.history && props.history.length ? (
					<List>
						{props.history.length &&
							props.history.map((entry, index) => (
								<ListItem
									key={index}
									onPress={() => handleEntryTap(entry.searchValue)}
								>
									<Body>
										<Text
											style={styles.entryTitle}
											numberOfLines={2}
											ellipsizeMode='tail'
										>
											{formatSearchValue(entry.searchValue)}
										</Text>
										<Text
											style={styles.entryTime}
											numberOfLines={1}
											ellipsizeMode='middle'
										>
											{formatTimestamp(entry.timestamp)}
										</Text>
									</Body>
								</ListItem>
							))}
					</List>
				) : (
					<View style={{ flex: 1, maxHeight: "100%" }}>
						<Text style={{ textAlign: "center" }}>
							You have no search history yet
						</Text>
					</View>
				)}
			</Content>
		</Container>
	);
};

const mapStateToProps = state => ({
	history: state.history.history,
});

export default connect(mapStateToProps)(HistoryScreen);

const styles = StyleSheet.create({
	title: { margin: 28, fontFamily: "Montserrat_SemiBold" },
	entryTitle: {
		fontFamily: "Montserrat_Medium",
		fontSize: 18,
	},
	entryTime: {
		fontFamily: "Montserrat_Light",
		fontSize: 16,
	},
});
