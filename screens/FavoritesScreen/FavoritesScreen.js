import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import {
	Container,
	Content,
	Text,
	List,
	ListItem,
	Body,
	H1,
} from "native-base";
import { connect } from "react-redux";

import { getFavorites } from "../../utilities/asyncStorageUtils";
import { setFavorites } from "../../redux/actions";
import { formatText, formatMinutes } from "../../utilities/formatUtils";

const FavoritesScreen = props => {
	useEffect(() => {
		getFavorites()
			.then(favorites => props.dispatch(setFavorites(favorites)))
			.catch(error => console.log(error));
	}, []);

	function handleRecipeTap(recipe) {
		props.navigation.navigate("FavoriteDetails", { recipeId: recipe._id });
	}

	return (
		<Container>
			<Content>
				<H1 style={styles.title}>Favorites</H1>
				{props.favorites && props.favorites.length ? (
					<List>
						{props.favorites.map((favorite, index) => (
							<ListItem key={index} onPress={() => handleRecipeTap(favorite)}>
								<Body>
									<Text
										style={styles.name}
										numberOfLines={2}
										ellipsizeMode='tail'
									>
										{formatText(favorite.name)}
									</Text>
									<Text style={{ color: "grey" }}>
										{formatMinutes(favorite.minutes)} to prepare
									</Text>
								</Body>
							</ListItem>
						))}
					</List>
				) : (
					<View style={{ flex: 1, maxHeight: "100%" }}>
						<Text style={{ textAlign: "center" }}>
							You have no favorites yet
						</Text>
					</View>
				)}
			</Content>
		</Container>
	);
};

const mapStateToProps = state => ({
	favorites: state.favorites.favorites,
});

export default connect(mapStateToProps)(FavoritesScreen);

const styles = StyleSheet.create({
	title: { margin: 28, fontFamily: "Montserrat_SemiBold" },
	name: {
		textTransform: "capitalize",
		fontSize: 18,
		fontFamily: "Montserrat_Medium",
	},
});
