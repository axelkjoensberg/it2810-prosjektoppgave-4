import React, { useState } from "react";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";
import useDeepCompareEffect from "use-deep-compare-effect";
import { ScrollView } from "react-native-gesture-handler";
import { List, ListItem, Text, Body, View } from "native-base";

import { getSearchCriteria } from "../../redux/selectors";
import { loadRecipes } from "../../utilities/loadUtils";
import { formatMinutes, formatText } from "./../../utilities/formatUtils";
import {
	nextRecipePage,
	setLoadModeAppend,
	setLoading,
	setHistory,
} from "../../redux/actions";
import { storeHistoryEntry } from "../../utilities/asyncStorageUtils";
import ErrorMessage from "../../components/ErrorMessage";

const SearchResults = props => {
	const [recipes, setRecipes] = useState([]);
	const [willLoad, setWillLoad] = useState(true);
	const [error, setError] = useState();

	useDeepCompareEffect(() => {
		if (props.searchCriteria.searchValue.length && !props.loading) {
			setError(undefined);
			setWillLoad(true);
			props.dispatch(setLoading(true));
			loadRecipes(props.searchCriteria, props.loadMode, recipes)
				.then(res => {
					storeHistoryEntry(props.searchCriteria)
						.then(hist => {
							props.dispatch(setHistory(hist));
						})
						.catch(error => setError(error));
					setRecipes(res);
				})
				.catch(error => setError(error))
				.finally(() => {
					props.dispatch(setLoading(false));
					setWillLoad(false);
				});
		}
	}, [props.searchCriteria, props.loadMode]);

	const handleScroll = event => {
		const evt = event.nativeEvent;
		const distanceFromBottom =
			evt.contentSize.height -
			(evt.layoutMeasurement.height + evt.contentOffset.y);
		if (!props.loading) {
			if (distanceFromBottom >= 200 && distanceFromBottom <= 250) {
				props.dispatch(nextRecipePage());
				props.dispatch(setLoadModeAppend());
			} else if (distanceFromBottom < 10) {
				props.dispatch(nextRecipePage());
				props.dispatch(setLoadModeAppend());
			}
		}
	};

	const handleRecipeTap = recipe => {
		props.navigation.navigate("RecipeDetails", { recipeId: recipe._id });
	};

	if (error) {
		return (
			<View style={{ flex: 1, maxHeight: "100%", padding: 8 }}>
				<ErrorMessage error={error} />
			</View>
		);
	}

	if (
		!props.loading &&
		props.searchCriteria.searchValue.length &&
		!recipes.length &&
		!willLoad
	) {
		return (
			<View style={{ flex: 1, maxHeight: "100%" }}>
				<Text style={{ textAlign: "center" }}>
					Sorry, there were no results
				</Text>
			</View>
		);
	}

	return (
		<ScrollView
			style={{ flex: 1, maxHeight: "100%" }}
			scrollEventThrottle={150}
			onScroll={event => handleScroll(event)}
		>
			{recipes && (
				<List>
					{recipes.map((recipe, index) => (
						<ListItem key={index} onPress={() => handleRecipeTap(recipe)}>
							<Body>
								<Text
									style={{
										textTransform: "capitalize",
										fontSize: 18,
										fontFamily: "Montserrat_Medium",
									}}
									numberOfLines={2}
									ellipsizeMode='tail'
								>
									{formatText(recipe.name)}
								</Text>
								<Text style={{ color: "grey" }}>
									{formatMinutes(recipe.minutes)} to prepare
								</Text>
							</Body>
						</ListItem>
					))}
				</List>
			)}
		</ScrollView>
	);
};

const mapStateToProps = state => ({
	searchCriteria: getSearchCriteria(state),
	loading: state.loading.loading,
	loadMode: state.recipeLoadMode.loadMode,
});

export default connect(mapStateToProps)(withNavigation(SearchResults));
