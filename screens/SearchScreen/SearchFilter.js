import React, { useState } from "react";
import { Icon, Button, Text, H3 } from "native-base";
import { View, StyleSheet } from "react-native";
import Dialog, {
	DialogContent,
	DialogTitle,
	FadeAnimation
} from "react-native-popup-dialog";
import Slider from "react-native-slider";

import Colors from "../../constants/Colors.js";
import {
	setMaxIngredients,
	setMaxMinutes,
	setMaxSteps,
	setLoadModeLoad,
	resetPage
} from "../../redux/actions.js";
import { formatMinutes } from "./../../utilities/formatUtils";

const SearchFilter = props => {
	const [localMaxIngredients, setLocalMaxIngredients] = useState(
		props.filters.maxIngredients
	);
	const [localMaxMinutes, setLocalMaxMinutes] = useState(
		props.filters.maxMinutes
	);
	const [localMaxSteps, setLocalMaxSteps] = useState(props.filters.maxSteps);

	const handleUseChoices = () => {
		props.dispatch(setMaxIngredients(localMaxIngredients));
		props.dispatch(setMaxMinutes(localMaxMinutes));
		props.dispatch(setMaxSteps(localMaxSteps));
		props.dispatch(setLoadModeLoad());
		props.dispatch(resetPage());
		props.close();
	};

	const clearFilters = () => {
		setLocalMaxIngredients(0);
		setLocalMaxMinutes(0);
		setLocalMaxSteps(0);
	};

	return (
		<Dialog
			visible={props.open}
			onTouchOutside={() => {
				props.close();
			}}
			dialogTitle={
				<DialogTitle title="Filter recipes by" hasTitleBar={false} />
			}
			dialogAnimation={new FadeAnimation({ animationDuration: 200 })}
			width={0.7}
		>
			<DialogContent>
				<View>
					<View style={{ marginVertical: 20 }}>
						<Text style={styles.filterTitle}>Max # ingredients</Text>
						<Text style={styles.filterValue}>
							{localMaxIngredients} ingredients
						</Text>

						<View
							style={{
								flexDirection: "row",
								flexWrap: "nowrap",
								alignItems: "center"
							}}
						>
							<Slider
								style={styles.slider}
								minimumValue={0}
								maximumValue={20}
								step={1}
								thumbTintColor={Colors.primary}
								minimumTrackTintColor={Colors.primary}
								maximumTrackTintColor={"#8b96d9"}
								value={localMaxIngredients}
								onValueChange={value => setLocalMaxIngredients(value)}
							/>
						</View>
					</View>
					<View style={{ marginVertical: 20 }}>
						<Text style={styles.filterTitle}>Max prep time</Text>
						<Text style={styles.filterValue}>
							{formatMinutes(localMaxMinutes)}
						</Text>
						<View
							style={{
								flexDirection: "row",
								flexWrap: "nowrap",
								alignItems: "center"
							}}
						>
							<Slider
								style={styles.slider}
								minimumValue={0}
								maximumValue={180}
								step={1}
								thumbTintColor={Colors.primary}
								minimumTrackTintColor={Colors.primary}
								maximumTrackTintColor={"#8b96d9"}
								value={localMaxMinutes}
								onValueChange={value => setLocalMaxMinutes(value)}
							/>
						</View>
					</View>
					<View style={{ marginVertical: 20 }}>
						<Text style={styles.filterTitle}>Max # steps</Text>
						<Text style={styles.filterValue}>{localMaxSteps} steps</Text>
						<View
							style={{
								flexDirection: "row",
								flexWrap: "nowrap",
								alignItems: "center"
							}}
						>
							<Slider
								style={styles.slider}
								minimumValue={0}
								maximumValue={50}
								step={1}
								thumbTintColor={Colors.primary}
								minimumTrackTintColor={Colors.primary}
								maximumTrackTintColor={"#8b96d9"}
								value={localMaxSteps}
								onValueChange={value => setLocalMaxSteps(value)}
							/>
						</View>
					</View>
					<View style={{ marginTop: 16 }}>
						<Button full onPress={() => handleUseChoices()}>
							<Text style={{ color: "white" }}>use choices</Text>
						</Button>
					</View>
					<View style={{ marginTop: 16, alignItems: "center" }}>
						<Button transparent onPress={clearFilters}>
							<Text style={{ color: Colors.primary }}>Clear filters</Text>
						</Button>
					</View>
				</View>
			</DialogContent>
		</Dialog>
	);
};

export default SearchFilter;

const styles = StyleSheet.create({
	filterTitle: {
		fontSize: 18,
		fontWeight: "400"
	},
	filterValue: {
		fontSize: 16
	},
	slider: {
		flex: 1
		//height:
	}
});
